#!/bin/bash

if [ ! -f /usr/bin/ansible ]; then
	echo "Installing ansible"
	sudo apt-get install software-properties-common
    sudo apt-get install python-pip --no-install-recommends
	sudo apt-add-repository --yes --update ppa:ansible/ansible
	sudo apt-get install --yes ansible
    sudo pip install pywinrm
fi

if [ ! -f ~/.ssh/id_rsa ]; then
	ssh-keygen -b 4096 -f ~/.ssh/id_rsa -N ""
	cat ~/.ssh/id_rsa.pub
	exit
fi

ANSIBLE_CONFIG=ansible.cfg ansible-playbook site.yml
