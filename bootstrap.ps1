
function Install-Choco {
    $exists = Test-Path -Path C:\ProgramData\chocolatey\bin\choco.exe
    if (!$exists) {
        iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
        Write-Host "Chocolatey installed, please rerun the script"
        exit
    }
}

function Install-Git {
    Install-Choco
    $exists = Test-Path -Path "C:\Program Files\Git\bin\git.exe"
    if (!$exists) {
        & choco install git
        Write-Host "Git installed, please rerun the script"
        exit
    }
}

function Install-SshCertificate {
    Install-Git
    $exists = Test-Path -Path "${env:HOMEPATH}\.ssh\id_ed25519"
    if (!$exists) {
        & "C:\Program Files\Git\usr\bin\ssh-keygen.exe" -t ed25519
        Write-Host ""
        Write-Host "Add the public key to bitbucket keys and restart script"
        Get-Content "${env:HOMEPATH}\.ssh\id_ed25519.pub"
        exit
    }
}

function Start-Bootstrap {
    $path = $MyInvocation.PSScriptRoot
    Install-Choco

#    if (Test-Path -Path "$path\scripts\elevated.ps1" -PathType Leaf) {
#        . ("$path\scripts\elevated.ps1")
#    }
#    elseif (Test-Path -Path "$path\bootstrap\scripts\elevated.ps1" -PathType Leaf) {
#        . ("$path\bootstrap\scripts\elevated.ps1")
#    }
#    else {
#        Install-Git
#        Install-SshCertificate
#        git clone https://xxxx bootstrap
#        Write-Host Check the ansible scripts and rerun the bootstrap script
#    }
}

$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())

if ($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Set-Executionpolicy -Scope Process Unrestricted
    Start-Bootstrap
    pause
}
else {
    # restart as Administrator
    $command = $MyInvocation.MyCommand.Path
    Start-Process powershell -Verb RunAs -ArgumentList "-noexit ${command}"
}
