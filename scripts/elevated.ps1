Write-Host Set execution policy to RemoteSigned
set-executionpolicy remotesigned


$dir = Split-Path -parent $MyInvocation.MyCommand.Path
Write-Host In directory: $dir


Write-Host Remove bloatware apps
& $dir\remove-appx.ps1


& $dir\enable-wsl.ps1
& $dir\ConfigureRemotingForAnsible.ps1


& $dir\prepare-ansible.ps1
Set-Location -Path $dir\..\ansible
# & C:\Ubuntu\ubuntu1804.exe run ./bootstrap.sh
