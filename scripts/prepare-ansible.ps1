
$dir = Split-Path -parent ( Split-Path -parent $MyInvocation.MyCommand.Path )
$ini = "$dir\ansible\hosts.ini"

if (![System.IO.File]::Exists($ini)) {
	$cr = Get-Credential -Message "Please provide credentials for ansible" -UserName $env:UserName

	$text = Get-Content $dir\templates\hosts.ini -Raw
	$text = $text -replace '{USERNAME}', $cr.GetNetworkCredential().Username
	$text = $text -replace '{PASSWORD}', $cr.GetNetworkCredential().Password

    Set-Content -Path $ini -Value $text
}
