
$i = Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
if ($i.State -ne 'Enabled') {
    Write-Host Enable WSL
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
}

$dir = Split-Path -parent ( Split-Path -parent $MyInvocation.MyCommand.Path )
$url = "https://aka.ms/wsl-ubuntu-1804"
$zip = "$dir\temp\Ubuntu-1804.zip"
$ubuntu = "C:\ProgramData\Ubuntu"

if (![System.IO.File]::Exists($zip)){
    Write-Host Downloading Ubuntu 18.04
    Invoke-WebRequest -Uri $url -OutFile $zip -UseBasicParsing
}

if (![System.IO.Directory]::Exists($ubuntu)){
    Write-Host Extracting Ubuntu package
    Expand-Archive $zip $ubuntu
}

if (![System.IO.Directory]::Exists("$ubuntu\rootfs")) {
    Write-Host Installing Ubuntu. When done, just exit the terminal
    & "$ubuntu\ubuntu1804.exe" install
}
